<?php
/**
 * Created by PhpStorm.
 * User: Minh
 * Date: 7/23/2016
 * Time: 9:32 AM
 */

require_once 'include/DBFunctions.php';
$db = new DBFunctions();

$response = array("error" => FALSE);

if (isset($_POST['email']) && isset($_POST['password'])) {
    
    $email = $_POST['email'];
    $password = $_POST['password'];
    
    if ($db->isUserExisted($email)) {
        $response["error"] = TRUE;
        $response["error_msg"] = "User already existed with " . $email;
        echo json_encode($response);
    } else {
        $user = $db->storeUser($email, $password);
        if ($user) {
            $response["error"] = FALSE;
            echo json_encode($response);
        } else {
            $response["error"] = TRUE;
            $response["error_msg"] = "Unknown error occurred in registration!";
            echo json_encode($response);
        }
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters email or password is missing!";
    echo json_encode($response);
}