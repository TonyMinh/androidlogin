<?php
/**
 * Created by PhpStorm.
 * User: Minh
 * Date: 7/23/2016
 * Time: 9:19 AM
 */

require_once 'include/DBFunctions.php';
$db = new DBFunctions();

$response = array("error" => FALSE);

if (isset($_POST['email']) && isset($_POST['password'])) {
    
    $email = $_POST['email'];
    $password = $_POST['password'];
    
    $user = $db->getUserByEmail($email, $password);

    if ($user != false) {
        $response["error"] = FALSE;
        $response["id"] = $user['id'];
        echo json_encode($response);
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Login credentials are wrong. Please try again!";
        echo json_encode($response);
    }
} else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters email or password is missing!";
    echo json_encode($response);
}