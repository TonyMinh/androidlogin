<?php

/**
 * Created by PhpStorm.
 * User: Minh
 * Date: 7/23/2016
 * Time: 9:05 AM
 */
class DBConnect
{
    private $conn;

    public function connect() {
        require_once 'include/Config.php';
        
        $this->conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);

        return $this->conn;
    }
}