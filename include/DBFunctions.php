<?php

/**
 * Created by PhpStorm.
 * User: Minh
 * Date: 7/23/2016
 * Time: 9:08 AM
 */
class DBFunctions
{
    private $conn;

    function __construct() {
        require_once 'DBConnect.php';
        $db = new DBConnect();
        $this->conn = $db->connect();
    }

    function db_get_row($sql){
        $result = mysqli_query($this->conn, $sql);
        $row = array();
        if (mysqli_num_rows($result) > 0){
            $row = mysqli_fetch_assoc($result);
        }
        return $row;
    }

    function db_get_list($sql){
        $data  = array();
        $result = mysqli_query($this->conn, $sql);
        while ($row = mysqli_fetch_assoc($result)){
            $data[] = $row;
        }
        return $data;
    }

    function db_close(){
        if ($this->conn){
            mysqli_close($this->conn);
        }
    }

    function db_create_sql($sql, $filter = array())
    {
        // Chuỗi where
        $where = '';

        // Lặp qua biến $filter và bổ sung vào $where
        foreach ($filter as $field => $value){
            if ($value != ''){
                $value = addslashes($value);
                $where .= "AND $field = '$value', ";
            }
        }

        // Remove chữ AND ở đầu
        $where = trim($where, 'AND');
        // Remove ký tự , ở cuối
        $where = trim($where, ', ');

        // Nếu có điều kiện where thì nối chuỗi
        if ($where){
            $where = ' WHERE '.$where;
        }

        // Return về câu truy vấn
        return str_replace('{where}', $where, $sql);
    }

    public function storeUser($email, $password) {

        $encode_password = md5($password);

        $stmt = $this->conn->prepare("INSERT INTO users(email, password) VALUES(?, ?)");
        $stmt->bind_param("ss", $email, $encode_password);
        $result = $stmt->execute();
        $stmt->close();
        
        if ($result) {
            $user = $this->db_get_row($this->db_create_sql('SELECT * FROM users {where}', array(
                'email' => $email
            )));

            return $user;
        } else {
            return false;
        }
    }

    public function getUserByEmail($email, $password) {

        $user = $this->db_get_row($this->db_create_sql('SELECT * FROM users {where}', array(
            'email' => $email
        )));

        $this->db_close();

        if($user['password'] == md5($password)) {
            return $user;
        } else {
            return null;
        }
    }

    public function isUserExisted($email) {
        $stmt = $this->conn->prepare("SELECT email from users WHERE email = ?");

        $stmt->bind_param("s", $email);

        $stmt->execute();

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    public function createEvent($id, $date, $time, $tag, $content) {
        $stmt = $this->conn->prepare("INSERT INTO events(id, date_event, time_event, tag, content) VALUES(?, ?, ?, ?, ?)");
        $stmt->bind_param("sssss", $id, $date, $time, $tag, $content);
        $result = $stmt->execute();
        $stmt->close();

        if ($result) {
            $event = $this->db_get_row($this->db_create_sql('SELECT * FROM events {where}', array(
                'id' => $id
            )));

            return $event;
        } else {
            return false;
        }
    }

    public function getEvent($id){
        $data  = array();
        $events = mysqli_query($this->conn, $this->db_create_sql("SELECT * FROM events {where}", array(
            'id' => $id
        ))) or die(mysqli_error());
        while ($row = mysqli_fetch_assoc($events)){
            $data[] = $row;
        }
        return $data;
    }

//    public function getEvent(){
//        $data  = array();
//        $events = mysqli_query($this->conn, "SELECT * FROM events") or die(mysqli_error());
//        foreach($events as $row){
//            $data[] = $row;
//        }
//        return $data;
//    }
}