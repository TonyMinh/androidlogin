<?php
/**
 * Created by PhpStorm.
 * User: MINH-PC
 * Date: 8/5/2016
 * Time: 12:15 PM
 */

require_once 'include/DBFunctions.php';
$db = new DBFunctions();

$response = array("error" => FALSE);

if (isset($_POST['id']) && isset($_POST['date']) && isset($_POST['time']) && isset($_POST['tag']) && isset($_POST['content'])) {

    $id = $_POST['id'];
    $date = $_POST['date'];
    $time = $_POST['time'];
    $tag = $_POST['tag'];
    $content = $_POST['content'];

    $event = $db->createEvent($id, $date, $time, $tag, $content);
    if ($event) {
        $response["error"] = FALSE;
        echo json_encode($response);
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknown error occurred in creating event!";
        echo json_encode($response);
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters are missing!";
    echo json_encode($response);
}