<?php
/**
 * Created by PhpStorm.
 * User: MINH-PC
 * Date: 8/5/2016
 * Time: 4:29 PM
 */

require_once 'include/DBFunctions.php';
$db = new DBFunctions();

$response = array(
    "error" => FALSE,
    "event" => array()
);

if (isset($_POST['id'])) {

    $id = $_POST['id'];

    $events = $db->getEvent($id);
    if ($events) {
        $response["error"] = FALSE;
        foreach ($events as $row) {
            $event = array();
            $event["date"] = $row["date_event"];
            $event["time"] = $row["time_event"];
            $event["tag"] = $row["tag"];
            $event["content"] = $row["content"];
            array_push($response["event"], $event);
        }
        echo json_encode($response);
    } else {
        $response["error"] = TRUE;
        echo json_encode($response);
    }

}else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Can't accept parameter id!";
    echo json_encode($response);
}